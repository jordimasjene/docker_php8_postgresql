# Formació DIBA per frameworks PHP8 + Symfony 5  

Programari necessari

    Composer 
    Docker 

Elements del Docker

    PHP     - versio 8
    Nginx   - Servidor web
 
Instal·lacio de Symfony 5.2 via composer

    - Amb versió de symfony:
        composer create-project symfony/website-skeleton:"^5.2" src

    - Amb la última versió de symfony: 
        composer create-project symfony/skeleton src